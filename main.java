class Main {
    public static void main(String[] args) {
        int p = 19;
        int value = Integer.parseInt(args[0]);
        int counter = 0;
        while (value % p == 0) {
            counter++;
            value /= p;
        }
        System.out.printf("%d,%d,Java,Did you know Java runs on 3 billion devices?", p, value);
    }
}